import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

/* NOTE: async attribute doesn't work for JSX mode. No clue yet. */
/* $loadScript will automatically add async for script. */
// import LoadScript from 'vue-plugin-load-script';
// Vue.use(LoadScript);

new Vue({
  render: h => h(App),
}).$mount('#app')

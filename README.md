# Demo_vue_load_React

React is also good for progressive mode. See the official doc: https://reactjs.org/docs/add-react-to-a-website.html


In this demo, React is combined with Vue. i.e, one ==React== button is inserted into the ==Vue== component.


## NO JSX in React Component
Here uses **vue-plugin-load-script** to load the ```<script>``` into Vue template.

Here I used two methods to inserts ```<script>```, but the outside one I picked vue-plugin-load-script, due to its promise feature to ensure the load sequence.

```javascript
    this.$loadScript('https://unpkg.com/react@16/umd/react.development.js')
    this.$loadScript('https://unpkg.com/react-dom@16/umd/react-dom.development.js')
    .then(() => {

      // <!-- Load our React component. -->
      let like_button = document.createElement('script');
      like_button.setAttribute('src','like_button.js');
      // like_button.defer = true;
      document.head.appendChild(like_button);

    })  
  },
```

I did tried for using script's attributes of aysnc and defer, but no luck. So, I figured out this way.


## JSX in React Component
In the branch of **jsx-in-react-in-vue**.

For progressive (kinda static) mode, the scripts holding JSX need to be declared as ```type="text/babel"```. Also, ```<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>``` is correspondingly needed.

In this demo, the configuration about importing scripts in above (using createelement while not jsx) method all failed. This should be realted to the VUE's init workflow. No deep digging yet. 

Fix is here. Just copy the script declaration of react, reactdom, babel in the index.html. Then everything goes smoothly. Even easier than above way. But this make the root index.html dirty. 




----
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
